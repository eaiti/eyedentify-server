import cognitive_face as CF
from flask import Flask, json, abort, flash, redirect, render_template, request, url_for, jsonify

CF.Key.set('3c8e963581b943bdbc8d62b02cbe9eb3')
CF.BaseUrl.set('https://eastus.api.cognitive.microsoft.com/face/v1.0')

def identify(image_url):
    try:
        detected = CF.face.detect(image_url, True, False, 'age,gender,emotion')
        faceid = detected[0]["faceId"]
        pool = CF.face.identify([faceid], 'eai')
        eyedentified = CF.person.get('eai', pool[0]['candidates'][0]['personId']) 
        data = {'name': eyedentified['name'], 'info': eyedentified['userData']}
        return data
    except CF.util.CognitiveFaceException:
        return None

# Not finished?
def add(images, name, info):
    CF.person.create('eai', name, )
    for image in images:
        CF.person.add_face(image, 'eai', info)
    CF.person_group.train('eai')

def initialize():
    CF.person_group.create('eai', "eai technologies")
    james_id = CF.person.create('eai', 'james anderson', '{"email" : "james@eaiti.com", "phone": "571-334-7705", "position": "application innovator", "team": "dotnet", "company": "eai technologies"}')
    nick_id = CF.person.create('eai', 'nick meier')#, {"email": "nick@eaiti.com", "phone": '949-285-0328‬', "position": "application innovator", "team": "java", "company": "eai technologies"})
    sufi_id = CF.person.create('eai', 'sufi nawaz')#, {"email": "sufi@eaiti.com", "phone": 'not set', "position": "application innovator", "team": "java", "company": "eai technologies"})
    james_faces = ['https://i.imgur.com/ZwYzSqL.jpg', 'https://i.imgur.com/gaNQcBG.jpg', 'https://i.imgur.com/PAdPX7H.jpg', 'https://i.imgur.com/oEBJDuc.jpg']
    nick_faces = ['https://i.imgur.com/H6Ltioo.jpg', 'https://i.imgur.com/hPJN8jt.jpg', 'https://i.imgur.com/BeqfNYA.jpg']
    sufi_faces = ['https://i.imgur.com/2kePwf8.jpg', 'https://i.imgur.com/kcXPthE.jpg', 'https://i.imgur.com/p3JeBOK.jpg', 'https://i.imgur.com/OqIuPIw.jpg', 'https://i.imgur.com/sv08II8.jpg']
    for image in james_faces:
        CF.person.add_face(image, 'eai', james_id['personId'])
    for image in nick_faces:
        CF.person.add_face(image, 'eai', nick_id['personId'])
    for image in sufi_faces:
        CF.person.add_face(image, 'eai', sufi_id['personId'])
    CF.person_group.train('eai') 
    
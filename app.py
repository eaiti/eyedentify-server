# import face_no_flask

import cv2

import os
import sys
import math
import numpy as np
import skimage.io
import matplotlib
import matplotlib.pyplot as plt
ROOT_DIR = os.path.abspath("./")
sys.path.append(os.path.join(ROOT_DIR, "samples/coco/"))  # To find local version
import coco
import mrcnn.model as modellib
from mrcnn import visualize
MODEL_DIR = os.path.join(ROOT_DIR, "logs")
COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")

def getFrame(video, offsetSeconds, frame_index):
  video.set(cv2.CAP_PROP_POS_MSEC, offsetSeconds * 1000)
  hasFrames, image = video.read()
  if hasFrames:
    results = model.detect([image])
    # print("-----test-----")
    # print(len(results))
    # cv2.imwrite("images/image"+str(frame_index)+".jpg", image)
    r = results[0]
    coco_class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
                   'bus', 'train', 'truck', 'boat', 'traffic light',
                   'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
                   'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
                   'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
                   'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
                   'kite', 'baseball bat', 'baseball glove', 'skateboard',
                   'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
                   'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
                   'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
                   'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
                   'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
                   'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
                   'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
                   'teddy bear', 'hair drier', 'toothbrush']
    # visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'], 
    #                         coco_class_names, r['scores'])
    image_name = "images/image"+str(frame_index)
    # print(os.path.join(ROOT_DIR, image_name))
    # skimage.io.imsave(os.path.join(ROOT_DIR, image_name), r)

    visualize.save_image(image, image_name, r['rois'], r['masks'],
    r['class_ids'],r['scores'],coco_class_names,
    filter_classs_names=['person'],scores_thresh=0.9,mode=0)

    # cv2.imwrite("images/image"+str(frame_index)+".jpg", newImage)
  return hasFrames

if __name__=='__main__':
    # Setup RCNN
    # Import COCO config
    sys.path.append(os.path.join(ROOT_DIR, "samples/coco/"))  # To find local version
    class InferenceConfig(coco.CocoConfig):
        # Set batch size to 1 since we'll be running inference on
        # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
        GPU_COUNT = 1
        IMAGES_PER_GPU = 1

    config = InferenceConfig()
    config.display()

    model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)
    model.load_weights(COCO_MODEL_PATH, by_name=True)


    # Get frames from video
    video_capture = cv2.VideoCapture('nickring.mp4')
    seconds = 0
    # frame_rate = .05
    frame_rate = 0.03333333333
    frame_index = 0
    success = True
    while success:
        success = getFrame(video_capture, seconds, frame_index) 
        frame_index += 1
        seconds += frame_rate
        if (seconds > 15):
          break
        else:
          print("current time: " + str(seconds))
        # seconds = round(seconds, 2)
